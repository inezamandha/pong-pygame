# Library Python
import pygame
import sys

from pygame.locals import *

pygame.init()

# Warna dari Pygame
black = (0,0,0)
white = (255,255,255)
red = (255,0,0)
green = (0,255,0)
blue = (46, 134, 193)
light_blue = (52, 152, 219)
orange = (243, 156, 18)

# Background Pygame
spaceImg = pygame.image.load('space.jpg')

# Icon Pygame
game_icon = pygame.image.load('ping-pong.png')
pygame.display.set_icon(game_icon)

# Gambar Keyboard yang diperlukan untuk bermain
arrow_keys = pygame.image.load('arrow_keys.png')
arrow_keys = pygame.transform.scale(arrow_keys, (300, 300))

# Gambar Keyboard Escape untuk Exit Game
esc_key = pygame.image.load('esc_key.png')
esc_key = pygame.transform.scale(esc_key, (100, 100))

# Gambar Keyboard 'P' untuk Pause Game
p_key = pygame.image.load('p_key.png')
p_key = pygame.transform.scale(p_key, (127,85))

# Size Layar Pygame
display_width = 800
display_height = 600

# Lebar & Tinggi Paddle
paddle_width = 100
paddle_height = 20

# Mengeluarkan Window Displaynya beserta Judul dari Pygame
gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('Pong')

def draw_paddle(gameDisplay, x, y, width, height):
    ''' Fungsi Membuat Paddle '''
    if x <= 0:
        x = 0
    if x >= display_width - paddle_width:
        x = display_width - paddle_width

    pygame.draw.rect(gameDisplay, blue, [x, y, width, height])

def draw_ball(gameDisplay, color, pos, radius):
    ''' Fungsi Membuat Ball '''
    pygame.draw.circle(gameDisplay, orange, pos, radius)

def button(text, x, y, width, height, inactive_color, active_color, action = None):
    ''' Fungsi Membuat Button pada Pygame '''
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x + width > mouse[0] > x and y + height > mouse[1] > y:
        pygame.draw.rect(gameDisplay, active_color, (x, y, width, height))
        if click[0] == 1 and action != None:
            if action == 'Quit Game':
                quit_game()
            if action == 'Start Game':
                start_game()
            if action == 'Help':
                help_game()
            if action == 'Quit':
                quit_game()
            if action == 'Continue':
                start_game()
            if action == 'Back':
                game_intro(gameDisplay)
            if action == 'Play Again':
                start_game()
            
    else:
        pygame.draw.rect(gameDisplay, inactive_color, (x, y, width, height))

    # Font pada Button dan penempatannya
    try:
        buttonTextFont = pygame.font.SysFont('Bahnschrift', 50)
    except:
        buttonTextFont = pygame.font.SysFont(None, 50)
        
    buttonText = buttonTextFont.render(text, True, white)
    buttonTextPos = buttonText.get_rect()
    buttonTextPos.center = ((x+(width/2)), (y+(height/2)))
    gameDisplay.blit(buttonText, buttonTextPos)

def paused():
    ''' Fungsi untuk Mempause Pygame '''
    # Font pada saat sedang dipause
    try:
        pausedTextFont = pygame.font.SysFont('Bahnschrift', 100)
    except:
        pausedTextFont = pygame.font.SysFont(None, 100)

    # Letak text
    pausedText = pausedTextFont.render('Paused', True, white)
    pausedTextPos = pausedText.get_rect()
    pausedTextPos.center = ((display_width/2), (display_height/4))
    gameDisplay.blit(pausedText, pausedTextPos)

    # Letak Button pada saat sedang dipause
    pause = True
    while pause:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game()
        
        button('Continue', 100, 450, 250, 75, blue, light_blue, 'Continue')
        button('Quit', 450, 450, 250, 75, blue, light_blue, 'Quit')

        pygame.display.update()

def game_over():
    ''' Fungsi untuk menampilkan Game Over saat kalah di PyGame '''
    # Font pada saat Game Over
    try:
        gameOverTextFont = pygame.font.SysFont('Bahnschrift', 100)
    except:
        gameOverTextFont = pygame.font.SysFont(None, 100)

    # Letak text       
    gameOverText = gameOverTextFont.render('Game Over', True, white)
    gameOverTextPos = gameOverText.get_rect()
    gameOverTextPos.center = ((display_width/2), (display_height/4))
    gameDisplay.blit(gameOverText, gameOverTextPos)

    # Letak Button saat Game Over
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game()
        
        button('Play Again', 100, 450, 250, 75, blue, light_blue, 'Play Again')
        button('Quit', 450, 450, 250, 75, blue, light_blue, 'Quit')

        pygame.display.update()

def help_game():
    ''' Fungsi untuk menampilkan bantuan di Pygame '''
    
    gameDisplay.blit(spaceImg, (0,0))

    # Font Large Text pada saat meminta bantuan
    try:
        helpTextFont = pygame.font.SysFont('Bahnschrift', 100)
        textFont = pygame.font.SysFont('Bahnschrift', 36)
    except:
        helpTextFont = pygame.font.SysFont(None, 100)
        textFont = pygame.font.SysFont(None, 36)

    # Letak text        
    helpText = helpTextFont.render('Help', True, white)
    helpTextPos = helpText.get_rect()
    helpTextPos.center = ((display_width/2), (display_height/4))
    gameDisplay.blit(helpText, helpTextPos)

    # Letak Gambar dan Text di Help
    ''' Arrow Keys '''
    gameDisplay.blit(arrow_keys, (50,200))
    
    text_1 = textFont.render('Left', True, white)
    text_1_pos = text_1.get_rect()
    text_1_pos.center = (100,450)
    gameDisplay.blit(text_1, text_1_pos)

    text_2 = textFont.render('Right', True, white)
    text_2_pos = text_2.get_rect()
    text_2_pos.center = (325,450)
    gameDisplay.blit(text_2, text_2_pos)

    ''' Escape Key '''
    gameDisplay.blit(esc_key, (450,250))

    text_3 = textFont.render('Escape', True, white)
    text_3_pos = text_3.get_rect()
    text_3_pos.center = (625,300)
    gameDisplay.blit(text_3, text_3_pos)

    ''' P Key '''
    gameDisplay.blit(p_key, (440,400))

    text_4 = textFont.render('Pause', True, white)
    text_4_pos = text_4.get_rect()
    text_4_pos.center = (625,435)
    gameDisplay.blit(text_4, text_4_pos)

    # Untuk keluar dari Pygame
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game()

        button('Back', 35, 35, 135, 75, blue, light_blue, 'Back')

        pygame.display.update()

def game_intro(gameDisplay):
    ''' Fungsi untuk Membuat Intro atau Tampilan Awal pada Pygame '''
    
    start = True
    while start:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game()
            
        gameDisplay.blit(spaceImg, (0,0))

        # Letak Font
        try:
            titleGameFont = pygame.font.SysFont('Bahnschrift', 100)
        except:
            titleGameFont = pygame.font.SysFont(None, 100)
        
        titleGame = titleGameFont.render('Pong', True, white)
        titleGamePos = titleGame.get_rect()
        titleGamePos.center = ((display_width/2), (display_height/4))
        gameDisplay.blit(titleGame, titleGamePos)

        # Membuat Button di Intro pada Game
        button('Start Game', 200, 250, 400, 80, blue, light_blue, 'Start Game')
        button('Help', 200, 350, 400, 80, blue, light_blue, 'Help')
        button('Quit Game', 200, 450, 400, 80, blue, light_blue, 'Quit Game')
        
        pygame.display.update()

def start_game():
    ''' Fungsi untuk memulai Pygame '''
    
    # Koordinat Paddle
    paddle_x = display_width/2
    paddle_y = display_height - 20

    # Koordinat Ball
    ball_x = 50
    ball_y = 50

    # Kecepatan Ball
    speed_ball_x = 5
    speed_ball_y = 5

    # Kecepatan Paddle
    speed_paddle_x = 0
    speed_paddle_y = 0

    # Score Counter
    score = 0
    
    # Game's Loop   
    running = True
    fpsClock=pygame.time.Clock()
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    speed_paddle_x = -6
                elif event.key == pygame.K_RIGHT:
                    speed_paddle_x = 6
                elif event.key == pygame.K_p:
                    paused()
                elif event.key == pygame.K_ESCAPE:
                    game_intro(gameDisplay)
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    speed_paddle_x = 0
                elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    speed_paddle_y = 0
                    
        gameDisplay.blit(spaceImg, (0,0))
        
        paddle_x += speed_paddle_x
        paddle_y += speed_paddle_y
        
        ball_x += speed_ball_x
        ball_y += speed_ball_y

        # Perpindahan Ball
        if ball_x < 0:
            ball_x = 0
            speed_ball_x *= -1
        elif ball_x > display_width - 10:
            ball_x = display_width - 10
            speed_ball_x *= -1
        elif ball_y < 0:
            ball_y = 0
            speed_ball_y *= -1
        elif ball_x > paddle_x and ball_x < paddle_x + paddle_width and ball_y == display_height - 25:
            speed_ball_y *= -1
            score += 1
        elif ball_y > display_height:
            speed_ball_y *= -1
            game_over()

        # Menggambar Ball dan Paddle
        draw_ball(gameDisplay, orange, [ball_x, ball_y], 10)
        
        draw_paddle(gameDisplay, paddle_x, paddle_y, paddle_width, paddle_height)

        # Hasil Score
        try:        
            scoreFont = pygame.font.SysFont('Bahnschrift', 30)
        except:
            scoreFont = pygame.font.SysFont(None, 30)

        scoreText = scoreFont.render("Score : " + str(score), True, white)
        gameDisplay.blit(scoreText,[50,50])    
           
        pygame.display.update()         
        fpsClock.tick(60)

def quit_game():
    ''' Fungsi untuk keluar dari Pygame '''
    pygame.quit()
    sys.exit()

# Memanggil Fungsi game_intro untuk menjalankan Pygame
game_intro(gameDisplay)
